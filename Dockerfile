FROM node:17-slim

ARG launch_version

WORKDIR /usr/src/app
USER root

ADD . .

RUN npm install --force
RUN npm run build

EXPOSE 8080

ENV HTTPS true

# Read default portal from the local environment
ENV REACT_APP_MERGE_PORTAL "$MERGE_PORTAL"
ENV REACT_APP_MERGE_API_URI "$MERGE_API_URI"
ENV REACT_APP_MERGE_AUTH_URI "$MERGE_AUTH_URI"
ENV REACT_APP_MERGE_LAUNCH_VERSION $launch_version

ENTRYPOINT npx react-inject-env set -d ./dist && npm run -d start
