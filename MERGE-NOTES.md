Merge Development Notes
=======================

# TLS

We inject enviroment at run time in the launch container instead oi build time. This way
we support running a single launch container against different portals at different URLS.
This works by generating a file at `/env.js` that holds the desired environment. This
file is read at run time.

So to run in dev mode, we need to generate this file and the ./dist directory. The runtime
will serve from / and ./dist which makes this work.

Note that you can run this in dev mode in a VTE but you have to set it up.

1. Make new localhost name for the dev instance in the same domain as it usally runs. Add
this to /etc/hosts:

```
127.0.0.1 devlaunch.mergetb.example.net
```

2. Update kratos to allow this new name in cors: `kubectl edit cm kratos-config`.
Add your new name to the `cors.allowed_origins` list.
```
 allowed_origins:
  - https://launch.mergetb.example.net
  - https://devlaunch.mergetb.example.net
```

3. Restart kratos to ge tthat change: `kubectl rollout restart deployment kratos`.
4. Set LAUNCH_URI in `.env`:

```
REACT_APP_LAUNCH_URI: 'https://devlaunch.mergetb.example.net'
```

5. If you're using a nonstandard port, check the -origins flag of the apiserver deployment
via `kubectl edit deployment apiserver` in spec.template.spec.containers.command:

```
- -origins
- https://*.mergetb.example.net,https://localhost:9000
```

```
# preflight. rerun if you update .env* at all.
mkdir ./dist
npx react-inject-env set -d ./dist   # read .env and generate ./dist/env.js

# run the application on the dev name on port 443
sudo -E HTTPS=true npm run start:dev -- --https --host devlaunch.mergetb.example.net --port 443
```



