import React from 'react';

interface DrawerContextType {
  activeDrawer: string | null;
  setActiveDrawer: React.Dispatch<React.SetStateAction<string | null>>;
  drawerWidth: number;
  setDrawerWidth: React.Dispatch<React.SetStateAction<number>>;
}

export const DrawerContext = React.createContext<DrawerContextType | undefined>(undefined);

export const useDrawerContext = () => {
  const context = React.useContext(DrawerContext);
  if (context === undefined) {
    throw new Error('useDrawerContext must be used within a DrawerContextProvider');
  }
  return context;
};
