import * as React from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import {
  Card,
  CardBody,
  LoginFooterItem,
  LoginMainFooterBandItem,
  LoginPage,
  ListItem,
  ListVariant,
  EmptyState,
  EmptyStateIcon,
  Spinner,
  Level,
  LevelItem,
  Avatar,
  Brand,
  Stack,
  StackItem,
  EmptyStateHeader,
  EmptyStateBody,
} from '@patternfly/react-core';
import ory from '@app/lib/OrySdk';
import { AuthContext } from '@app/lib/AuthProvider';

import { useTranslation } from 'react-i18next';

import launchLogo from '@app/bgimages/launch-logo.svg';

import nsfLogo from '@app/bgimages/nsf-logo-60.png';
import dhsLogo from '@app/bgimages/dhs-logo-60.png';
import darpaLogo from '@app/bgimages/darpa-logo-75.png';
import arpaeLogo from '@app/bgimages/arpa-e-logo.png';

import { LoginFlow, UpdateLoginFlowBody } from '@ory/client';
import { UserAuthCard } from '@ory/elements';
import { sdkError } from '@app/lib/OrySdk';

const MergeLoginPage: React.FunctionComponent = () => {
  const [flow, setFlow] = React.useState<LoginFlow>();
  const history = useHistory();
  const { setSession, isAuthenticated } = React.useContext(AuthContext);

  const { t } = useTranslation();

  if (isAuthenticated) {
    // already logged in.
    history.push('/');
  }

  const params = React.useMemo(() => {
    return new URLSearchParams(document.location.search);
  }, []);
  const aal2 = params.get('aal2');
  const loginChallenge = params.get('login_challenge');
  const returnTo = params.get('return_to');

  // initialize the sdkError for generic handling of errors
  const sdkErrorHandler = sdkError(undefined, setFlow, '/recovery');

  // Get the flow based on the flowId in the URL (.e.g redirect to this page after flow initialized)
  const getFlow = React.useCallback(
    (flowId: string) =>
      ory
        // the flow data contains the form fields, error messages and csrf token
        .getLoginFlow({ id: flowId })
        .then(({ data: flow }) => setFlow(flow))
        .catch(sdkErrorHandler),
    [sdkErrorHandler]
  );

  // Create a new login flow
  const createFlow = React.useCallback(
    () =>
      ory
        .createBrowserLoginFlow({
          refresh: true,
          aal: aal2 ? 'aal2' : 'aal1',
          ...(loginChallenge && { loginChallenge: loginChallenge }),
          ...(returnTo && { returnTo: returnTo }),
        })
        // flow contains the form fields and csrf token
        .then(({ data: flow }) => {
          // Update URI query params to include flow id
          const params = new URLSearchParams({ ['flow']: flow.id });
          history.replace({ pathname: location.pathname, search: params.toString() });
          // Set the flow data
          setFlow(flow);
        })
        .catch(sdkErrorHandler),
    [sdkErrorHandler, aal2, returnTo, history, loginChallenge]
  );

  // submit the login form data to Ory
  const submitFlow = (body: UpdateLoginFlowBody) => {
    // something unexpected went wrong and the flow was not set
    if (!flow) return history.push('/login', { replace: true });

    // we submit the flow to Ory with the form data
    ory
      .updateLoginFlow({ flow: flow.id, updateLoginFlowBody: body })
      .then(({ data: session }) => {
        setSession(session.session);
        // we successfully submitted the login flow, so lets redirect to the dashboard
        history.push('/', { replace: true });
      })
      .catch(sdkErrorHandler);
  };

  React.useEffect(() => {
    // we might redirect to this page after the flow is initialized, so we check for the flowId in the URL
    const flowId = params.get('flow');
    // the flow already exists
    if (flowId) {
      getFlow(flowId).catch(createFlow); // if for some reason the flow has expired, we need to get a new one
      return;
    }

    // we assume there was no flow, so we create a new one
    createFlow();
  }, [params, getFlow, createFlow]);

  // TODO: Add Terms of Use and Privacy Policy
  const footerListItems = (
    <React.Fragment>
      <Stack hasGutter>
        <StackItem>
          <Level hasGutter>
            <ListItem>
              <LoginFooterItem href="https://mergetb.gitlab.io/testbeds/sphere/sphere-docs/docs/experimentation/">
                Documentation
              </LoginFooterItem>
            </ListItem>
            <ListItem>
              <LoginFooterItem href="https://mergetb.gitlab.io/testbeds/sphere/sphere-docs/docs/experimentation/hello-world-gui/">
                HOWTO
              </LoginFooterItem>
            </ListItem>
            <ListItem>
              <LoginFooterItem href="https://chat.mergetb.net/mergetb">Chat</LoginFooterItem>
            </ListItem>
            <ListItem>
              <LoginFooterItem href="https://gitlab.com/mergetb">Source Code</LoginFooterItem>
            </ListItem>
          </Level>
        </StackItem>
        <StackItem isFilled></StackItem>
        <StackItem>
          <Level hasGutter>
            <LevelItem>
              <Brand src={nsfLogo} alt="NSF" />
            </LevelItem>
            <LevelItem>
              <Brand src={dhsLogo} alt="DHS" />
            </LevelItem>
            <LevelItem>
              <Brand src={darpaLogo} alt="DARPA" />
            </LevelItem>
            <LevelItem>
              <Brand src={arpaeLogo} alt="ARPA-E" />
            </LevelItem>
          </Level>
        </StackItem>
      </Stack>
    </React.Fragment>
  );

  const signUpForAccountMessage = (
    <LoginMainFooterBandItem>
      Need an account? <Link to="/registration">Sign up.</Link>
    </LoginMainFooterBandItem>
  );

  const forgotCredentials = (
    <LoginMainFooterBandItem>
      <Link to="/recovery">Forgot username or password?</Link>
    </LoginMainFooterBandItem>
  );

  return flow ? (
    <LoginPage
      loginTitle=""
      footerListItems={footerListItems}
      footerListVariants={ListVariant.inline}
      brandImgSrc={launchLogo}
      brandImgAlt="Merge Launch"
      backgroundImgSrc="/images/bgimage.png"
      textContent={'Welcome to Launch, a portal to the DeterLab testbeds powered by MergeTB.'}
      signUpForAccountMessage={signUpForAccountMessage}
      forgotCredentials={forgotCredentials}
    >
      {flow ? (
        <UserAuthCard
          title={t('login_message')}
          flow={flow}
          flowType={'login'}
          includeScripts={false}
          onSubmit={({ body }) => submitFlow(body as UpdateLoginFlowBody)}
        />
      ) : (
        <Spinner />
      )}
    </LoginPage>
  ) : (
    <EmptyState variant="full">
      <EmptyStateHeader icon={<EmptyStateIcon icon={Spinner} />} />
      <EmptyStateBody>Contacting Authorization Server</EmptyStateBody>
    </EmptyState>
  );
};

export { MergeLoginPage };
