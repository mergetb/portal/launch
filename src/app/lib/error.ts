type GRPCError = {
  code: number;
  message: string;
  details: any;
};

export { GRPCError };
