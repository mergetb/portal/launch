// This file defines TypeScript interfaces for the tutorial system, including:
// 	• TutorialStep: Represents a single step in a tutorial
// Tutorial: Represents a complete tutorial


export interface TutorialStep {
  title: string;
  content: string;
  imageUrls?: string[];
  expectedAction?: string;
  successFeedback?: string;
  failureFeedback?: string;

  // For single additional action
  additionalAction?: string;
  additionalSuccessFeedback?: string;
  additionalFailureFeedback?: string;
  
  // For multiple additional actions
  additionalActions?: string[];
  additionalFeedback?: { [key: string]: string };
}

export interface Tutorial {
  id: string;
  title: string;
  description: string;
  estimatedTime: string;
  steps: TutorialStep[];
}